<?php

declare(strict_types=1);
require(__DIR__ . '/vendor/autoload.php');

if (ini_get('zend.assertions') < 1) {
	fprintf(STDERR, "zend.assertions must be 1\n");
	exit(2);
}

ini_set('assert.active', '1');
ini_set('assert.warning', '1');
ini_set('assert.bail', '1');
ini_set('display_errors', 'on');
error_reporting(E_ALL);

$exit = 1;

register_shutdown_function(function() use(&$exit): void {
	exit($exit);
});

$loop = React\EventLoop\Factory::create();
$resolver = (new React\Dns\Resolver\Factory)->create('8.8.8.8', $loop);
$ping = new Adawolfa\ICMP\Ping($loop, $resolver);
$mask = 0b0000;

$ping->ping('localhost', .5)->then(function(float $time) use(&$mask): void {
	$mask |= 0b1000;
	printf("ping(localhost): OK\n");
}, function(Exception $exception): void {
	assert(false, (string) $exception);
});

$ping->ping('10.99.99.99', .5)->then(function(float $time): void {
	assert(false, (string) $time);
}, function(Exception $exception) use(&$mask): void {
	printf("ping(10.99.99.99): OK\n");
	assert($exception);
	$mask |= 0b0100;
});

$count = 0;

$stop = $ping->periodic('127.0.0.1', function(float $time, Exception $exception = null) use(&$mask, &$stop, &$count): void {

	if ($count === 3) {
		printf("ping(127.0.0.1) [3]: OK\n");
		assert($exception);
		assert('Interrupted.' === $exception->getMessage());
		$mask |= 0b0010;
	} else {
		printf("ping(127.0.0.1) [%d]: OK\n", $count);
		assert($exception === null);
	}

	if (++$count === 3) {
		$stop();
	}

}, .1, .5);

$count2 = 0;

$stop2 = $ping->periodic('10.99.99.99', function(float $time, Exception $exception = null) use(&$mask, &$stop2, &$count2): void {

	if ($count2 === 3) {
		printf("ping(10.99.99.99) [3]: OK\n");
		assert($exception);
		assert('Interrupted.' === $exception->getMessage());
		$mask |= 0b0001;
	} else {
		printf("ping(127.0.0.1) [%d]: OK\n", $count2);
		assert($exception);
	}

	if (++$count2 === 3) {
		$stop2();
	}

}, .1, .5);

$loop->run();

assert(0b1111 === $mask, (string) $mask);
$exit = 0;